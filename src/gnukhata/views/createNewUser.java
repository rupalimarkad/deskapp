package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.StartupController;
import gnukhata.controllers.accountController;

import java.text.NumberFormat;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.sun.org.apache.bcel.internal.generic.LLOAD;

public class createNewUser extends Composite  {
	
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;

	static Display display;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strype;
	
	Label lblUserName;
	Text txtUserName;
	Label lblPassword;
	Text txtPassword;
	Label lblConfirmPassword;
	Text txtConfirmPassword;
	Label lblRole;
	Combo drpdwnRole;
	Label lblSecurityQuestion;
	Combo cmbSecurityQuestion;
	Text txtSecurityQuestion;
	Label lblAnswer;
	Text txtAnswer;
	Button btnSave;
	Button btnCancel;
	
	boolean msgToggleFlag = false;

	int role;
	String question;
	String answer;
	
	Vector<Object> params;
	protected int[] orgNameList;
    public createNewUser(Composite parent,int style) 
	{
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		MainShell.lblLogo.setVisible(false);
		 MainShell.lblLine.setVisible(false);
		 MainShell.lblOrgDetails.setVisible(false);
		strToYear =  globals.session[3].toString();
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		lblOrgDetails.setLayoutData(layout);

		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblLogo , 2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
		Label lblNewUser = new Label(this, SWT.NONE);
		lblNewUser.setText("Create New User");
		lblNewUser.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,7);
		layout.left = new FormAttachment(40);
		//layout.right = new FormAttachment(65);
		//layout.bottom = new FormAttachment(36);
		lblNewUser.setLayoutData(layout);
		
		lblUserName = new Label(this, SWT.NONE);
		lblUserName.setText("&User Name :");
		lblUserName.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblNewUser,15);
		layout.left = new FormAttachment(35);
		lblUserName.setLayoutData(layout);
		
		txtUserName = new Text(this, SWT.BORDER);
		txtUserName.setFont(new Font(display,"Times New Romen",10,SWT.NORMAL));
		txtUserName.setText("");
		layout = new FormData();
		layout.top = new FormAttachment(lblNewUser,15);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(65);
		txtUserName.setLayoutData(layout);
		//txtUserName.setFocus();
		
		
		lblPassword = new Label(this, SWT.NONE);
		lblPassword.setText("&Password :");
		lblPassword.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblUserName,20);
		layout.left = new FormAttachment(35);
		lblPassword.setLayoutData(layout);
		
		txtPassword = new Text(this, SWT.BORDER);
		txtPassword.setFont(new Font(display,"Times New Romen",10,SWT.NORMAL));
		txtPassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lblUserName,20);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(65);
		txtPassword.setLayoutData(layout);
		
		lblConfirmPassword = new Label(this, SWT.NONE);
		lblConfirmPassword.setText("C&onfirm Password :");
		lblConfirmPassword.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblPassword,20);
		layout.left = new FormAttachment(35);
		lblConfirmPassword.setLayoutData(layout);
		
		txtConfirmPassword = new Text(this, SWT.BORDER);
		txtConfirmPassword.setFont(new Font(display,"Times New Romen",12,SWT.NORMAL));
		txtConfirmPassword.setEchoChar('*');
		layout = new FormData();
		layout.top = new FormAttachment(lblPassword,20);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(65);
		txtConfirmPassword.setLayoutData(layout);
		
		lblRole = new Label(this, SWT.NONE);
		lblRole.setText("S&elect Role:");
		lblRole.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblConfirmPassword,20);
		layout.left = new FormAttachment(35);
		lblRole.setLayoutData(layout);
		
		
		drpdwnRole = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY);
		drpdwnRole.setFont(new Font(display,"Times New Romen",10,SWT.NORMAL));
		layout = new FormData();
		layout.top = new FormAttachment(lblConfirmPassword,20);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(65);
		drpdwnRole.setLayoutData(layout);
		drpdwnRole.add("Manager");
		drpdwnRole.select(0);		
		drpdwnRole.add("Operator");
		
		lblSecurityQuestion = new Label(this, SWT.NONE);
		lblSecurityQuestion.setText("Security &Question :");
		lblSecurityQuestion.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblRole,20);
		layout.left = new FormAttachment(35);
		//layout.right = new FormAttachment(63);
		//layout.bottom = new FormAttachment(62);
		lblSecurityQuestion.setLayoutData(layout);
		//lblSecurityQuestion.setVisible(false);

		cmbSecurityQuestion = new Combo(this, SWT.BORDER | SWT.READ_ONLY);
		cmbSecurityQuestion.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		layout= new FormData();
		layout.top = new FormAttachment(lblRole,20);
		layout.left = new FormAttachment(52);
		//layout.right = new FormAttachment(65);
		cmbSecurityQuestion.add("What is your Birth Date?");
		cmbSecurityQuestion.select(0);	
		cmbSecurityQuestion.add("Which is your Favourite fruit?");
		//cmbSecurityQuestion.select(1);
		cmbSecurityQuestion.add("What is Lucky Number?");
		//cmbSecurityQuestion.select(2);
		cmbSecurityQuestion.add("Please Enter Your Own Question?");

		cmbSecurityQuestion.setLayoutData(layout);
		cmbSecurityQuestion.setVisible(true);
		
		//cmbSecurityQuestion.select(0);
		
		
		
		
		
		txtSecurityQuestion = new Text(this, SWT.BORDER);
		txtSecurityQuestion.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout= new FormData();
		layout.top = new FormAttachment(cmbSecurityQuestion,10);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(65);
		txtSecurityQuestion.setLayoutData(layout);
		txtSecurityQuestion.setVisible(false);
		txtSecurityQuestion.setEnabled(false);
		
		lblAnswer = new Label(this, SWT.NONE);
		lblAnswer.setText("Answer :");
		lblAnswer.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(txtSecurityQuestion,20);
		layout.left = new FormAttachment(35);
		//layout.right = new FormAttachment(63);
		//layout.bottom = new FormAttachment(62);
		lblAnswer.setLayoutData(layout);
		//lblSecurityQuestion.setVisible(false);


		txtAnswer= new Text(this, SWT.BORDER);
		txtAnswer.setFont(new Font(display,"Times New Roman",10,SWT.NORMAL));
		layout= new FormData();
		layout.top = new FormAttachment(txtSecurityQuestion,10);
		layout.left = new FormAttachment(52);
		layout.right = new FormAttachment(65);
		txtAnswer.setLayoutData(layout);

		
		btnSave = new Button(this,SWT.PUSH);
		btnSave.setText("&Save");
		btnSave.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblAnswer,10);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(47);
		btnSave.setLayoutData(layout);
		btnSave.setEnabled(false);
		
		
		btnCancel = new Button(this,SWT.PUSH);
		btnCancel.setText("&Cancel");
		btnCancel.setFont(new Font(display, "Times New Roman", 12, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblAnswer,10);
		layout.left = new FormAttachment(btnSave,50);
		layout.right = new FormAttachment(65);
		btnCancel.setLayoutData(layout);
		
		
	
		this.getAccessible();
		this.setEvents();
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
        BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		
		globals.setThemeColor(this, Background, Foreground);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        
		txtUserName.setBackground(FocusBackground);
		txtUserName.setForeground(FocusForeground);
		this.pack();
	
}
    
    
    private void setEvents()
	{
    
		txtUserName.setFocus();

		
		txtUserName.addTraverseListener(new TraverseListener() {
			
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.detail==SWT.TRAVERSE_MNEMONIC)
					{
						arg0.doit=false;
						return;
					}
				
				if(arg0.detail==SWT.TRAVERSE_RETURN || arg0.detail==SWT.TRAVERSE_TAB_NEXT)
				{
					if(txtUserName.getText().trim().equals(""))
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("Please Enter a User Name");
						alert.open();
						arg0.doit=false;
						return;
					}
					
					if(!StartupController.userExists(txtUserName.getText()))
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("The User Name already exists");
						alert.open();
						arg0.doit=false;
						return;
					}
				
				}
				txtPassword.setFocus();
				}
		});
		
				
		txtPassword.addTraverseListener(new TraverseListener() {
			
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.detail==SWT.TRAVERSE_MNEMONIC)
				{
					arg0.doit=false;
					return;
				}
			
			if(arg0.detail==SWT.TRAVERSE_RETURN || arg0.detail==SWT.TRAVERSE_TAB_NEXT)
			{
				if(txtPassword.getText().trim().equals(""))
				{
					MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
					alert.setText("Error!");
					alert.setMessage("Please Enter Password");
					alert.open();
					arg0.doit=false;
					return;
				}
				txtConfirmPassword.setFocus();
					
			}
			
			if(arg0.detail==SWT.TRAVERSE_TAB_PREVIOUS || arg0.detail==SWT.TRAVERSE_ARROW_PREVIOUS)
			{
				txtUserName.setFocus();	
				return;
			
				
			}
		
				
				
			}
		});
		
			
		txtConfirmPassword.addTraverseListener(new TraverseListener() 
		{
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.detail==SWT.TRAVERSE_MNEMONIC)
				{
					arg0.doit=false;
					return;
				}
			
			if(arg0.detail==SWT.TRAVERSE_RETURN || arg0.detail==SWT.TRAVERSE_TAB_NEXT)
			{
				if(txtConfirmPassword.getText().trim().equals(""))
				{
					MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
					alert.setText("Error!");
					alert.setMessage("Please Confirm your Password");
					alert.open();
					arg0.doit=false;
					return;
				}
				
				if(!txtPassword.getText().equals(txtConfirmPassword.getText()))
				{
					MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
					alert.setText("Error!");
					alert.setMessage("Password Doesnt Match");
					alert.open();
					arg0.doit=false;
					return;
				}
			
			}
			if(arg0.detail==SWT.TRAVERSE_TAB_PREVIOUS || arg0.detail==SWT.TRAVERSE_ARROW_PREVIOUS)
			{
				txtPassword.setFocus();
				return;
			}
		
			drpdwnRole.setFocus();	
			}
		});
		
				    	
		drpdwnRole.addTraverseListener(new TraverseListener() {
			
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.detail==SWT.TRAVERSE_MNEMONIC)
				{
					arg0.doit=false;
					return;
				}
			
			if(arg0.detail==SWT.TRAVERSE_RETURN || arg0.detail==SWT.TRAVERSE_TAB_NEXT)
			{
				if(drpdwnRole.getSelectionIndex()>= 0  )
				{
					cmbSecurityQuestion.setFocus();
					return;
				}
			
			}
			
			if(arg0.detail==SWT.TRAVERSE_TAB_PREVIOUS || arg0.detail==SWT.TRAVERSE_ARROW_PREVIOUS)
			{
				txtConfirmPassword.setFocus();
				return;
			}
				
				
			}
		});
		
		
				
		
	
		/*cmbSecurityQuestion.addFocusListener(new FocusAdapter() {
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusGained(arg0);
			cmbSecurityQuestion.setListVisible(true);
			return;
		
		}
		
		});
		
		cmbSecurityQuestion.addTraverseListener(new TraverseListener() {
			
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.detail==SWT.TRAVERSE_MNEMONIC)
				{
					arg0.doit=false;
					return;
				}
			
			if(arg0.detail==SWT.TRAVERSE_RETURN || arg0.detail==SWT.TRAVERSE_TAB_NEXT)
			{
				
				if(cmbSecurityQuestion.getSelectionIndex()==3)
				{
					txtSecurityQuestion.setVisible(true);
					txtSecurityQuestion.setEnabled(true);
					txtSecurityQuestion.setFocus();
					return;
				}
				else
				{
					
					txtAnswer.setFocus();
					return;
				}
		}
			if(arg0.detail==SWT.TRAVERSE_TAB_PREVIOUS || arg0.detail==SWT.TRAVERSE_ARROW_PREVIOUS)
			{
				drpdwnRole.setFocus();
				return;
			}
		
			}
		});*/
		cmbSecurityQuestion.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				
				
				txtAnswer.setFocus();
				return;
			}
			
			@Override
				public void focusGained(FocusEvent arg0) {
				
		
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
				cmbSecurityQuestion.setListVisible(true);
								
				if(txtSecurityQuestion.isEnabled())
				{
					txtSecurityQuestion.setVisible(false);
					txtSecurityQuestion.setEnabled(false);
					return;
				}
				
				
				}
			
			});
			
			
			
			cmbSecurityQuestion.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
					{

						if(cmbSecurityQuestion.getSelectionIndex()==3)
						{
							txtSecurityQuestion.setVisible(true);
							txtSecurityQuestion.setEnabled(true);
							txtSecurityQuestion.setFocus();
							return;
						}
						else
						{
							
							txtAnswer.setFocus();
							return;
						}
					
					}
					if(e.keyCode==SWT.ARROW_UP)
					{
						drpdwnRole.setFocus();
						return;
					}
				}
			});

			cmbSecurityQuestion.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
			
					if(cmbSecurityQuestion.getSelectionIndex()==3)
					{
						txtSecurityQuestion.setVisible(true);
						txtSecurityQuestion.setEnabled(true);
						txtSecurityQuestion.setFocus();
						return;
					}
					else
					{
						//cmbSecurityQuestion.setListVisible(true);
						txtSecurityQuestion.setVisible(false);
						txtSecurityQuestion.setEnabled(false);
						txtAnswer.setFocus();
						return;
					}
				}
				});

		
		txtSecurityQuestion.addTraverseListener(new TraverseListener() {
			
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.detail==SWT.TRAVERSE_MNEMONIC)
				{
					arg0.doit=false;
					return;
				}
			
			if(arg0.detail==SWT.TRAVERSE_RETURN || arg0.detail==SWT.TRAVERSE_TAB_NEXT)
			{
				if(txtSecurityQuestion.isEnabled())
				{
					if(txtSecurityQuestion.getText().trim().equals(""))
					{
						MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
						alert.setText("Error!");
						alert.setMessage("Please Enter your Question");
						alert.open();
						arg0.doit=false;
						return;
					}
					
				}
				txtAnswer.setFocus();
				
			
			}
			if(arg0.detail==SWT.TRAVERSE_TAB_PREVIOUS || arg0.detail==SWT.TRAVERSE_ARROW_PREVIOUS)
			{
				cmbSecurityQuestion.setFocus();
				txtSecurityQuestion.setVisible(false);
				txtSecurityQuestion.setEnabled(false);
				return;

			}
	
			}
		});
		
		txtAnswer.addTraverseListener(new TraverseListener() {
			
			@Override
			public void keyTraversed(TraverseEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.detail==SWT.TRAVERSE_MNEMONIC)
				{
					arg0.doit=false;
					return;
				}
			
			if(arg0.detail==SWT.TRAVERSE_RETURN || arg0.detail==SWT.TRAVERSE_TAB_NEXT)
			{
				if(txtAnswer.getText().trim().equals(""))
				{
					MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
					alert.setText("Error!");
					alert.setMessage("Please Enter Answer");
					alert.open();
					arg0.doit=false;
					return;
				}
					//btnSave.setEnabled(true);
					btnSave.setFocus();
					return;
			}
			
			if(arg0.detail==SWT.TRAVERSE_TAB_PREVIOUS || arg0.detail==SWT.TRAVERSE_ARROW_PREVIOUS)
			{
				if(cmbSecurityQuestion.getSelectionIndex()==3)
				{   txtSecurityQuestion.clearSelection();
					txtSecurityQuestion.setFocus();
					return;
				}
				else
				{
					cmbSecurityQuestion.setFocus();
					return;
				
				}
			}
					
			}
		});
		
				
		txtAnswer.addFocusListener(new FocusAdapter() {
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusGained(arg0);
			btnSave.setEnabled(true);
			return;
		}
					@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
			
			if(txtAnswer.getText().trim().equals(""))
			{
				btnSave.setEnabled(false);
				txtAnswer.setFocus();
				return;
			}
			
			
			
		}
		});
		
		
		
		
					
		btnSave.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e)
			{
				if(e.keyCode==SWT.ARROW_RIGHT)
				{
					btnCancel.setFocus();
				}
				if(e.keyCode==SWT.ARROW_UP)
				{	
					txtAnswer.setFocus();
					btnSave.setEnabled(false);
				}
			}
		});
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				role = drpdwnRole.getSelectionIndex();
				if (cmbSecurityQuestion.getSelectionIndex()==3)
				{
					question=txtSecurityQuestion.getText().toString();
				}
				else
				{
				question = cmbSecurityQuestion.getItem(cmbSecurityQuestion.getSelectionIndex());
				}
				answer= txtAnswer.getText();
				if(txtUserName.getText().trim().equals(""))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg1.setText("Alert!");
					msg1.setMessage("Enter username");
					msg1.open();
					
					txtUserName.setFocus();
					return;
				}
				if(!StartupController.userExists(txtUserName.getText()))
				{
	
					MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					msg.setText("Error!");
					msg.setMessage("User Name is not Unique");
					msg.open();
					txtUserName.setText("");
					txtUserName.setFocus();
					arg0.doit=false;
					return;
					
				
				}
				
				if(txtPassword.getText().trim().equals(""))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg1.setText("Alert!!");
					msg1.setMessage("Enter password");
					msg1.open();
					
					txtPassword.setFocus();
					return;
				}
				if(txtConfirmPassword.getText().trim().equals(""))
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg1.setText("Alert!!");
					msg1.setMessage("Enter confirmed password");
					msg1.open();
					
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtConfirmPassword.setFocus();
							return;
						}
					});
				}
				if(! txtConfirmPassword.getText().trim().equals(""))
				{

					if(!txtPassword.getText().equals(txtConfirmPassword.getText()))
					{
					
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK |SWT.ICON_INFORMATION);
					msg1.setText("Information!");
					msg1.setMessage("Password doesnt match");
					msg1.open();
					//txtConfirmPassword.selectAll();
					display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							txtConfirmPassword.setFocus();
							return;

						}
					});
					}
				
					

					
				}
								if(cmbSecurityQuestion.getSelectionIndex()== -1)
				{
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg1.setText("Information!");
					msg1.setMessage("Select Security Question");
					msg1.open();
					cmbSecurityQuestion.setFocus();
					return;
					
				}
				
				if(txtSecurityQuestion.isEnabled())
				{
					
					if(txtSecurityQuestion.getText().trim().equals(""))
					{
						MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg1.setText("Information!");
						msg1.setMessage("Please Enter The Question");
						msg1.open();
						txtSecurityQuestion.setFocus();
						return;
						
					}
			
				}
								
				if(txtPassword.getText().equals(txtConfirmPassword.getText()))
				{
					StartupController.createuser(txtUserName.getText(), txtPassword.getText(), role,question,answer);
					MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					msg1.setText("Information");
						msg1.setMessage("User Created");
						msg1.open();
						btnSave.getShell().getDisplay().dispose();
						MainShell ms = new MainShell(display);
						return;
				}
				
				
				
			}
				
		});
	btnSave.addFocusListener(new FocusAdapter() {
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			//super.focusGained(arg0);
			if(txtAnswer.getText().trim().equals(""))
			{
				MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
				msg1.setText("Information!");
				msg1.setMessage("Please Enter The Answer");
				//txtAnswer.setFocus();
				msg1.open();
				display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						
						txtAnswer.setFocus();
						return;
					}
				});
				
							}


		}
	
	});
	
	
		btnCancel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnSave.setFocus();
				}
			}
		});
		
				btnCancel.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					MessageBox msgConfirm = new MessageBox(new Shell(), SWT.YES| SWT.NO| SWT.ICON_QUESTION );
					msgConfirm.setText("Confirm!");
					msgConfirm.setMessage("Are you sure you want to cancel");
					int answer = msgConfirm.open();
					if(answer == SWT.YES)
					{
						
						btnCancel.getShell().getDisplay().dispose();
						MainShell ms = new MainShell(display);
					}
					if(answer== SWT.NO)
					{
						txtUserName.setFocus();
					}
					
				}
				});
			/*btnCancel.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					//super.focusGained(arg0);
					if(txtSecurityQuestion.isEnabled())
					{
						if(txtSecurityQuestion.getText().trim().equals(""))
						{
							MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
							msg1.setText("Alert!");
							msg1.setMessage("Please Enter Security Question");
							//msg1.open();
							//txtConfirmPassword.selectAll();
							display.getCurrent().asyncExec(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									txtSecurityQuestion.setFocus();
									return;
								
								}
							});
						}
					}
					
					if(txtAnswer.getText().trim().equals(""))
					{
						MessageBox msg1 = new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
						msg1.setText("Alert!");
						msg1.setMessage("Please Enter Answer");
						//msg1.open();
						
						display.getCurrent().asyncExec(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								txtAnswer.setFocus();
								return;
							}
						});
					
					}
				}
				
			});*/
			
	}


    public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}
	
    protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
}
